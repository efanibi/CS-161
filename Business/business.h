#include <iostream>
const double delta=.001;
using namespace std;

struct rentalcar
{
    string name;
    double mileprice;
    double  dayprice;
};

struct food
{
    double breakfast;
    double Lunch;
    double  Dinner;
};

struct feesandrefunds
{
 double totalparking;
 double totalgas;
 double totalcarent;
 double totalmealcost;
 double totalhotelcost;
 double mealrefund;
 double parkingrem;
 double gasrem;
 double rentaldailyrem;
 double hotelrem;
};

#ifndef BUSINESS_H
#define BUSINESS_H
/**
 * @brief carselect Prompts User to select what type of car they used during the trip
 * @param mycar struct that is the rental car, or taxi
 * @param days number of days the trip lasted for
 * @param mytrip struct with the trip we are looking at
 */
void carselect(rentalcar &mycar,int days,feesandrefunds &mytrip);
/**
 * @brief rentalcarmax Finds the maximimum per day and maximum per mile, and uses that to calcute the refund giving
 * @param cartype Parameter used to find what the maximums are
 * @param days multiple by values given by cartype to find the rental refund
 * @param mycar The car used during the trip, and has the maximum per mile and per day values stored in it
 * @param mytrip Has the property  total car refund, and total gas refund stored in it.
 */
void rentalcarmax(int cartype,int days,rentalcar &mycar,feesandrefunds &mytrip);
/**
 * @brief parking Find the parking refund or taxi refund based on carselect
 * @param mytrip Were the values of the total cost and total refund per catergory are stored
 * @param mycar THe car we used to find the prompts, and potental refunds per instance
 * @param days instance equals days so the number of days control how many instances for refunds their are.
 */
void parking(feesandrefunds &mytrip, rentalcar mycar,int days);
/**
 * @brief rentalparkrefund parking finds the total cost of the parking or taxi during the trip, here we get the specific amount that can be refunded.
 * Based on the max
 * @param mytrip the correct trip we are going on, same through out the program
 * @param parking array holding the values for how much we spend on parking per day
 * @param days number of days are trip was, so we know how many times to loop though array
 * @param mycar the car for the trip stays the same
 */
void rentalparkrefund(feesandrefunds &mytrip,double parking[],int days,rentalcar mycar);
/**
 * @brief invalidint prevents the program from crashing or looping endlessly when invalid inputs are enter, converts negatives. Only non-numbers are
 * considered "invalid"
 * @param input what the user has inputted into the console
 * @param message The message we want the user to see, repeated if invalid input is entered.
 */
void invalidint( int& input,string message);
/**
@brief invaliddouble prevents the program from crashing or looping endlessly when invalid inputs are enter, converts negatives. Only non-numbers are
* considered "invalid"
* @param input what the user has inputted into the console
* @param message The message we want the user to see, repeated if invalid input is entered.
*/
void invaliddouble( double& input,string message);
/**
 * @brief calctripcost Find the total cost of the trip, and the total reimbursment
 * @param totaltripcost the current total trip cost
 * @param totaltriprembursment the current total trip reimbursment
 * @param changetripcost what we are changing the tripcost by
 * @param changereimbursment what we are changing the reimbursment by
 */
void calctripcost(double &totaltripcost,double &totaltriprembursment,double changetripcost,double changereimbursment);
/**
 * @brief mealdata find the data for the deperature and arrival days, as timing controls what meals count for those days
 * @param days value that is passed onto mealsdaily
 * @param mytrip holds the total cost and total refunds, as one of its properties
 * @param myfood food for the current day, where the values of the three meals are held. Which is the price
 */

void mealdata(int days,feesandrefunds &mytrip,food myfood);
/**
 * @brief mealdaily find the total for the now arrival, non deperature days or days in the middle for a trip
 * @param days used to find how many times we should ask for all 3 meals.
 * @param mytrip  holds the total cost and refund for food
 * @param myfood where we store the 3 individual meals per day
 */
void mealdaily(int days,feesandrefunds &mytrip, food myfood);
/**
 * @brief mealdeperture Calculates refund for the day we leave for a trip, based on when we leave
 * @param mealD Gives a value so we can know what meals count, and then messages about the ones that do not count
 * @param mytrip holds the total refund and cost per catergory
 * @param myfood we we store the values for the meals on a specific day
 */
void mealdeperture(int mealD,feesandrefunds &mytrip, food myfood);
/**
 * @brief mealarrival Calculates refund for the day we leave for a trip, based on when we come back
 * @param mealA Gives a value so we can know what meals count, and then messages about the ones that do not count
 * @param mytrip holds the total refund and cost per catergory
 * @param myfood we we store the values for the meals on a specific day
 */
void mealarrival(int mealA,feesandrefunds &mytrip, food myfood);
/**
 * @brief mealmax used by other functions to find the max refund per meal, based on what meal it is i.e breakfast, lunch,etc
 * @param myfood we pass this so we can tell it what meal we are looking at
 */

void mealmax(food &myfood);
/**
 * @brief tripbase sets a base value for trip refund and cost
 * @param day number of days the trip is, stays the same after user inputs it in the beginning
 * @param mytrip stores the total refund, and total cost based on catergory
 * @return we return this value to main, and add it to the calculate tripcost, to find the total cost and refund of the trip.
 */

double tripbase(int day,feesandrefunds &mytrip);
/**
 * @brief repeatset allows for a value to be set to change a boolean value. Which controls whether the program is repeated.
 * @return returns the value so the boolean can be changed.
 */
int repeatset();


#endif // BUSINESS_H
