/*
 * Calculate Business Expenses, and how much will be reimbursed based on user inputs
 */
#include <business.h>

int main()
{
    bool repeat=1;
    while(repeat)
    {
    double totalcost=0;
    double totalremimbursment=0;
    rentalcar mycar;
    feesandrefunds mytrip;
    food myfood;
    int days=0;

    while(days<=0)
        invalidint(days,"How many days was your trip\n");

    double mytripbase=tripbase(days,mytrip);
    calctripcost(totalcost,totalremimbursment,mytripbase,(mytripbase-mytrip.totalhotelcost+mytrip.hotelrem));

    //get data about rental car, including daily and gas reimbursments
    carselect(mycar,days,mytrip);
    //Get data about fees and refund for car
    parking(mytrip,mycar,days);
    //Total Car refund and cost
     calctripcost(totalcost,totalremimbursment,mytrip.totalparking+mytrip.totalgas+mytrip.totalcarent,(mytrip.gasrem+mytrip.parkingrem+mytrip.rentaldailyrem));
    //Meal Refund
    mealdata(days,mytrip,myfood);
     //Total meal Cost=Total meal refund
     calctripcost(totalcost,totalremimbursment,mytrip.totalmealcost,mytrip.mealrefund);

    cout<<"Your total expense for this trip: "<<totalcost<<endl;
    cout<<"Your total reimbursement for this trip is: "<<totalremimbursment<<endl;
    repeat=repeatset();
}











}
