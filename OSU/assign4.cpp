#include <iostream>
#include <iomanip>
//This Program uses loops and conditionals to calculate the number of Months to pay off a loan

using namespace std;

int main()
{
    cout << fixed << setprecision(2);

    //Variables for User to Enter, and also run the program
    double Balance;
    cout<<"Enter balance: ";
    cin>>Balance;

    double Payment;
    cout<<"Enter Payment: ";
    cin>>Payment;

    double APR;
    cout<<"Enter APR: : ";
    cin>>APR;

    double TrueAPR=(APR/100)/12;
    double Interest;

    int Month=0;

    cout<<"Month"<< '\t'<<"Int."<< '\t'<<"Pay"<< '\t'<<"Balance"<<endl;
    cout<<"0"<< '\t'<< '\t'<< '\t'<<Balance<<endl;
    // While Loop Because we don't know how many times the loop will have to cycle, and we want to do a conditional.
    while(Balance>0)
    {   //Condition if else, because we need to account for the last month where we don't want to overpay
        Interest=Balance*TrueAPR;
        Balance=Interest+Balance;


         if(Balance<Payment)
            {Payment=Balance;
            Month=Month+1;
            Balance=Balance-Payment;
            cout<<Month<< '\t'<<Interest<< '\t'<<Payment<< '\t'<<Balance<<endl;}

        else
            {Month=Month+1;
         Balance=Balance-Payment;
         cout<<Month<< '\t'<<Interest<< '\t'<<Payment<< '\t'<<Balance<<endl;}

}



}
