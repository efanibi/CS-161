#ifndef WORDSEARCH_H
#define WORDSEARCH_H

#include <string>

#include <cstdlib>
#include <ctime>
using namespace std;
//Using namespace std in a .h can cause problems in a larger project
//It will be fine here
//Define these constatnts in the .h so they are available everywhere
// the .h is included
const int ROWS = 12;
const int COLS = 10;
/**
 * @brief printKey Prints out the location of words that will be hidden.
 * @param grid what we use to decide what characters to print out, has rows and columns and specific words are all out through the grind.
 */
void printKey(const char grid[ROWS][COLS]);

/**
 * @brief printPuzzle Turn Spaces into Random Letters, and print out
 * @param grid The grid with spaces we enter or have inputted.
 */
void printPuzzle(const char grid[ROWS][COLS]);
/**
 * @brief countSpaces Count the Number of Spaces we have in our Key or any puzzle inputted
 * @param grid The answer key or puzzle with a number of blank spaces
 * @return Total number of spaces in integers
 */
int countSpaces(const char grid[ROWS][COLS]);
/**
 * @brief checkHorizontalFit Make sure a Word will fit
 * @param grid A two way array or grid we enter
 * @param startRow Where to start the word going (row)
 * @param startCol Where to start the word going (column)
 * @param word Word we should check
 * @return Whether it fits or not
 */
bool checkHorizontalFit(const char grid[ROWS][COLS],
 int startRow,
 int startCol,
 const string& word);
/**
 * @brief placeHorizontal Make sure a Word will fit
 * @param grid A two way array or grid we enter
 * @param startRow Where to start the word going (row)
 * @param startCol Where to start the word going (column)
 * @param word word we should check
 * @return This Actually changes the puzzle if it is true, Otherwise puzzle is not outputted
 */
bool placeHorizontal(char grid[ROWS][COLS],
 int startRow, int startCol, const string& word);
/**
 * @brief placeDiagonal Inputs word, does not check if fits. Overlap is possible
 * @param grid A two way array or grid we enter
 * @param startRow Where to start the word going (row)
 * @param startCol Where to start the word going (column)
 * @param word word we should check
 */
void placeDiagonal(char grid[ROWS][COLS],
 int startRow, int startCol, const string& word);
/**
 * @brief Find the Largest Gaps in each line, only prints the location of the first match if tie
 * @param grid A two way array or grid we enter
 */
void printLargestGaps(const char grid[ROWS][COLS]);
/**
 * @brief makemaxRay Makes Array with location of gaps, on each row. Along with how large.
 * @param grid Most likely answer key with empty gaps or '_'
 * @param puzzle used to copy the information, an array
 */
void makemaxRay(char grid[ROWS][COLS],const char puzzle[ROWS][COLS]);

#endif // WORDSEARCH_H
