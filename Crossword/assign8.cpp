#include <iostream>


#include "wordSearch.h"

using namespace std;


int main()
{

//Hard-Coded EXample
     char puzzle[ROWS][COLS] = {
     {'H','_','_','_','_','_','_','_','_','_'},
     {'E','_','_','_','_','_','_','_','_','_'},
     {'A','_','D','_','_','_','I','N','T','_'},
     {'P','R','O','G','R','A','M','_','_','_'},
     {'_','_','U','_','_','_','_','_','_','_'},
     {'_','_','B','_','_','_','M','_','_','_'},
     {'_','_','L','_','_','_','A','_','_','_'},
     {'_','_','E','_','V','O','I','D','_','_'},
     {'_','_','_','_','_','_','_','_','_','_'},
     {'S','T','A','C','K','_','_','_','_','_'}
    };
    //User input


     //cout<<"Enter the Key"<<endl;
    //char puzzle[ROWS][COLS];
//    for(int row=0;row<ROWS;row++)
//    {
//        for(int col=0;col<COLS;col++)

//        {
//            cin>>puzzle[row][col];
//        }
//}



    //Spaces Function
    int spaces=countSpaces(puzzle);
    cout<<spaces<<endl;

   //Horizontal Fit Test 1 = True, and 0 = False
   bool fits=checkHorizontalFit(puzzle,0,1,"STACKPAN");
   cout<<"Does the Word Fit Horizontally: "<<fits<<endl;

  //Place Word Horizontally
  bool placeword=placeHorizontal(puzzle,9,0,"STACKPAN");
  //cout<<"Does the Word Fit Horizontally: "<<fits<<endl;

  //Diagonal
   placeDiagonal(puzzle,2,2,"DDDDDDD");

 //Print Puzzle
 printKey(puzzle);
 printPuzzle(puzzle);



  //Functions to Find Max Gaps
  char max[ROWS][COLS]={{0}};
  makemaxRay(max,puzzle);
  printLargestGaps(max);










}
