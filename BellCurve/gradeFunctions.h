#include <string>
#include <iostream>
#include <math.h>
#ifndef GRADEFUNCTIONS_H
#define GRADEFUNCTIONS_H


/**
 * @brief getArrayData input data into array
 * @param scores the score individual students got
 * @param size how many student
 *@return nothing but input to array
*/


void getArrayData(int scores[], int size);
/**
 * @brief average calulate the average
 * @param scores the score individual students got
 * @param size how many student
 * @return average
 */
double average(const int scores[], int size);
/**
 * @brief We use this to solve standard dev
 * @param scores the score individual students got
 * @param size how many student
 * @param mean average of the score
 * @return  the standard dev
 */
double standardDeviation(const int scores[], int size, double mean);
/**
 * @brief print student numbers, based on the number of student in problem
 * param size how many student their are and how much to print
 */
void printstudent(int size);
/**
 * @brief getLetterGrade what letter grade student got based on curve
 * @param scores the score individual students got
 * @param mean average of the score
 * @param stdDev
 * @return
 */
char getLetterGrade(int score, double mean, double stdDev);


#endif // GRADEFUNCTIONS_H
