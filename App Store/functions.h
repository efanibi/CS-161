#include <string>
#include <iostream>
#include  <fstream>
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
using namespace std;

//Read online and this seems to be one way to do a global struct, as adding it to main. Does not work
struct googleapp{
string name;
string catergory;
double ratings;
int reviews;
double size;
string installs;
string type;
string price;
string content;
string genres;
string lastupdated;
string currentversion;
string androidversion;
};

#ifndef FUNCTIONS_H
#define FUNCTIONS_H


/**
 * @brief readcsv create an array using the google playstore csv, use data from main to create the size of the array. Use Conditionals for data
 * that is mostly one type, but has i.e mostly int but a few string types in csv table
 * @param appdata this is the empty array that we will fill using the csv
 * @param rows this is gathered from main by using getline and counting the number of lines until the end of the file.
 * This way we can just scrape the playstore data, and not have to recode the numbers
 */
void readcsv(googleapp appdata[], int rows);
/**
 * @brief convertcsv convert some strings into ints, so they can be parse easier
 * @param appdata array we will changed
 * @param rows number of rows so we can work with files even if they get updated in the future
 */
void convertcsv(googleapp appdata[], int rows);
/**
 * @brief printrandom Print some random apps
 * @param appdata where we get the apps from
 * @param rows number of rows we can work with
 */

void printrandom(const googleapp appdata[],int rows);

/**
 * @brief printtopapps print off the top apps, stick to the first 1bout 1000 now. Until parsing of invalid or non-integer records is improved
 * @param appdata where we get data from
 * @param rows how many rows the array has
 */
void printtopapps(const googleapp appdata[],int rows);

/**
 * @brief printtopcatapps print off the top apps, stick to the first 1bout 1000 now. Until parsing of invalid or non-integer records is improved
 * @param appdata where we get data from
 * @param rows how many rows the array has
 */
void printtopcatapps(const googleapp appdata[],int rows);
/**
 * @brief printtoppaid Shows percentage of apps that are paid, and have large number of downloads
 * @param appdata where the data comes from
 * @param rows the number of rows in array
 */

void printtoppaidapps(const googleapp appdata[],int rows);


#endif // FUNCTIONS_H
